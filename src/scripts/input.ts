/**
 * Checks for possible collisions
 */
export const collisionCheck = (
  currentCoordinates: { orientation: string; Y: number; X: number },
  commandKey: string
) => {
  const orientation = currentCoordinates.orientation;

  /* North collision check */
  if (orientation === 'N') {
    if (commandKey === 'F') {
      return currentCoordinates.Y === 9 ? true : false;
    }
  }

  /* East collision check */
  if (orientation === 'E') {
    if (commandKey === 'F') {
      return currentCoordinates.X === 9 ? true : false;
    }
  }

  /* South collision check */
  if (orientation === 'S') {
    if (commandKey === 'F') {
      return currentCoordinates.Y === 0 ? true : false;
    }
  }

  /* West collision check */
  if (orientation === 'W') {
    if (commandKey === 'F') {
      return currentCoordinates.X === 0 ? true : false;
    }
  }

  return false;
};

export const decipherLog = (logCommand: string, type: string) => {
  let newItem = {};

  switch (type) {
    case 'info':
      newItem = {
        command: `Sending command: ${logCommand}`,
        id: Date.now(),
      };
      break;
    case 'collision':
      newItem = {
        command: 'WARNING! Collision Detected. Maintaining Position.',
        id: Date.now(),
      };
      break;
    default:
      newItem = {
        command: 'Unexpected Command. Not Sent.',
        id: Date.now(),
      };
  }
  return newItem;
};

import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders app', () => {
  render(<App />);
  const command = screen.getByText(/Rover Command/i);
  expect(command).toBeInTheDocument();
  const position = screen.getByText(/Rover Position/i);
  expect(position).toBeInTheDocument();
  const history = screen.getByText(/Command History/i);
  expect(history).toBeInTheDocument();
});

import { useContext } from 'react';
import MovementContext from '../context/movementContext';

const CommandHistory = () => {
  const { items } = useContext(MovementContext);
  const history = items.slice().reverse();

  return (
    <div className="commandHistory">
      <ul data-testid="commandLog">
        {history.map((item: { id: number; command: string }) => (
          <li data-testid="logEntry" key={item.id}>
            {item.command}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CommandHistory;

import { useContext } from 'react';
import MovementContext from '../context/movementContext';

const RoverLocation = () => {
  const { coordinates } = useContext(MovementContext);

  return (
    <>
      <h3>Rover Position</h3>
      <ul>
        <li>
          <strong>X:</strong> <span data-testid="Xposition">{coordinates.X}</span>
        </li>
        <li>
          <strong>Y:</strong> <span data-testid="Yposition">{coordinates.Y}</span>
        </li>
        <li>
          <strong>Orientation:</strong> <span data-testid="currentOrientation">{coordinates.orientation}</span>
        </li>
      </ul>
    </>
  );
};

export default RoverLocation;

import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from 'App';

beforeEach(() => {
  const div: any = document.createElement('div');
  render(<App />, div);
});

describe('Standard Protocol', () => {
  it('always lands at 0 0 N', () => {
    expect(screen.getByTestId('Yposition').innerHTML).toContain(0);
    expect(screen.getByTestId('Xposition').innerHTML).toContain(0);
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('N');
  });

  it("changes it's position incrementally by 1", () => {
    userEvent.click(screen.getByTestId('forward'));
    expect(screen.getByTestId('Yposition').innerHTML).toContain(1);
    userEvent.click(screen.getByTestId('right'));
    userEvent.click(screen.getByTestId('forward'));
    expect(screen.getByTestId('Xposition').innerHTML).toContain(1);
  });
});

describe('Turning', () => {
  it('Left turns the rover 90 degrees left', () => {
    userEvent.click(screen.getByTestId('left'));
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('W');
  });

  it('Right turns the rover 90 degrees right', () => {
    userEvent.click(screen.getByTestId('right'));
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('E');
  });

  it('Right turns the rover 180 degrees', () => {
    userEvent.click(screen.getByTestId('right'));
    userEvent.click(screen.getByTestId('right'));
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('S');
    userEvent.click(screen.getByTestId('left'));
    userEvent.click(screen.getByTestId('left'));
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('N');
  });
});

describe('Moving Forward', () => {
  it('Facing North increases Y position by 1', () => {
    userEvent.click(screen.getByTestId('forward'));
    expect(screen.getByTestId('Yposition').innerHTML).toContain(1);
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('N');
  });

  it('Facing South decreases Y position by 1', () => {
    userEvent.dblClick(screen.getByTestId('left'));
    userEvent.click(screen.getByTestId('forward'));
    expect(screen.getByTestId('Yposition').innerHTML).toContain(0);
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('S');
  });

  it('Facing East increases X position by 1', () => {
    userEvent.click(screen.getByTestId('right'));
    userEvent.click(screen.getByTestId('forward'));
    expect(screen.getByTestId('Xposition').innerHTML).toContain(1);
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('E');
  });

  it('Facing West decreases X position by 1', () => {
    userEvent.click(screen.getByTestId('left'));
    userEvent.click(screen.getByTestId('forward'));
    expect(screen.getByTestId('Xposition').innerHTML).toContain(0);
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('W');
  });
});

describe('Test it', () => {
  it('use the string "rfflfflfrff":', () => {
    userEvent.click(screen.getByTestId('right'));
    userEvent.dblClick(screen.getByTestId('forward'));
    userEvent.click(screen.getByTestId('left'));
    userEvent.dblClick(screen.getByTestId('forward'));
    userEvent.click(screen.getByTestId('left'));
    userEvent.click(screen.getByTestId('forward'));
    userEvent.click(screen.getByTestId('right'));
    userEvent.dblClick(screen.getByTestId('forward'));
    expect(screen.getByTestId('Xposition').innerHTML).toContain(1);
    expect(screen.getByTestId('Yposition').innerHTML).toContain(4);
    expect(screen.getByTestId('currentOrientation').innerHTML).toContain('N');
  });
});

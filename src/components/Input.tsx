import { useState, useContext, useEffect, useCallback } from 'react';
import { collisionCheck, decipherLog } from 'scripts/input';
import MovementContext from '../context/movementContext';

const Input = () => {
  const { items, setItems, coordinates, setCoordinates } = useContext(MovementContext);
  const [command, setCommand] = useState('');

  const handleKeyboard = (event: KeyboardEvent) => {
    event.preventDefault();
    const validCommands = ['F', 'L', 'R'];
    const key = event.key.toUpperCase();

    if (validCommands.find(element => element === key)) {
      setCommand(key);
    }
  };

  useEffect(() => {
    window.addEventListener('keyup', handleKeyboard);

    return () => {
      window.removeEventListener('keyup', handleKeyboard);
    };
  }, []);

  /**
   *
   * @param {string} logCommand
   * @param {string} type
   *
   * Updates visible log with the latest command
   */
  const fireLogEntry = useCallback(
    (logCommand, type) => {
      const newItem = decipherLog(logCommand, type);
      setItems([...items, newItem]);
    },
    [items, setItems]
  );

  /**
   *
   * @param {string} input
   *
   * Executes and logs command if collisionCheck returns false
   */
  const fireCommand = useCallback(
    input => {
      let collisionDetected = collisionCheck(coordinates, input);

      if (collisionDetected) {
        setCoordinates({ type: 'C' });
        fireLogEntry('C', 'collision');
      } else {
        setCoordinates({ type: input });
        fireLogEntry(input, 'info');
      }
    },
    [coordinates, fireLogEntry, setCoordinates]
  );

  useEffect(() => {
    if (command.length !== 0) {
      fireCommand(command);
      setCommand('');
    }
    return () => undefined;
  }, [command, fireCommand]);

  return (
    <div data-testid="controlZone">
      <button data-testid="forward" onClick={() => setCommand('F')}>
        F
      </button>
      <button data-testid="left" onClick={() => setCommand('L')}>
        L
      </button>
      <button data-testid="right" onClick={() => setCommand('R')}>
        R
      </button>
    </div>
  );
};

export default Input;

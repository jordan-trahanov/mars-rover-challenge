import React, { useState, useReducer, useEffect, useRef } from 'react';
import movement from './reducers/movement';
import MovementContext from './context/movementContext';
import CommandHistory from './components/CommandHistory';
import Input from './components/Input';
import RoverLocation from './components/RoverLocation';
import chevronNorth from './chevrons/bottom-chevron.png';
import chevronEast from './chevrons/right-chevron.png';
import chevronWest from './chevrons/left-chevron.png';
import chevronSouth from './chevrons/top-chevron.png';

const IconImage = new Image(18, 18);

const canvasGrid = (xParam = 0, yParam = 0, orientation = 'N') => {
  const canvas = document.getElementById('canvas') as HTMLCanvasElement;
  if (canvas) {
    var ctx = canvas.getContext('2d');
    if (ctx) {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.fillStyle = 'green';
      IconImage.src =
        orientation === 'N'
          ? chevronNorth
          : orientation === 'E'
          ? chevronEast
          : orientation === 'S'
          ? chevronSouth
          : chevronWest;

      var xvalue = 0;
      var yvalue = 0;

      for (var y = 0; y < 10; y++) {
        for (var x = 0; x < 10; x++) {
          ctx.fillRect(xvalue, yvalue, 18, 18);
          xvalue += 20;
        }
        xvalue = 0;
        yvalue += 20;
      }

      ctx.moveTo(xParam * 20, yParam * 20);
      setTimeout(() => {
        ctx?.drawImage(IconImage, xParam * 20, yParam * 20);
      }, 0);
    }
  }
};

const App = () => {
  const [coordinates, setCoordinates] = useReducer(movement, {
    X: 0,
    Y: 0,
    orientation: 'N',
  });
  const [items, setItems] = useState([]);
  const canvasRef: any = useRef();

  useEffect(() => {
    const { orientation } = coordinates as any;
    IconImage.src =
      orientation === 'N'
        ? chevronNorth
        : orientation === 'E'
        ? chevronEast
        : orientation === 'S'
        ? chevronSouth
        : chevronWest;
  }, [coordinates]);

  useEffect(() => {
    if (canvasRef.current) {
      canvasGrid(coordinates.X, coordinates.Y, coordinates.orientation);
    }
  }, [coordinates]);

  return (
    <MovementContext.Provider value={{ coordinates, setCoordinates, items, setItems }}>
      <div className="commandContainer">
        <h3>Rover Command</h3>
        <Input />
        <h3>Grid</h3>
        <canvas id="canvas" width="198" height="198" ref={canvasRef} />
        <RoverLocation />
        <h3>Command History</h3>
        <CommandHistory />
      </div>
    </MovementContext.Provider>
  );
};

export default App;

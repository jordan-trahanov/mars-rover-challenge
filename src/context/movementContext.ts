import React from 'react';

const MovementContext = React.createContext({}) as any;

export default MovementContext;
